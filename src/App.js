import './App.css';
import {useEffect, useState} from "react";
import Post from "./components/Post/Post";
import Form from "./components/Form/Form";

const url = 'http://146.185.154.90:8000/messages';
const App = () => {

    const [messages, setMessages] = useState([]);
    const [message, setMessage] = useState('');

    useEffect(() => {
        const interval = setInterval(() => {
            const fetchData = async () => {
                const dateTime = new Date();
                dateTime.setHours(dateTime.getHours() - 7);
                const currentDate = new Date(dateTime.toString().split('GMT')[0] + ' UTC').toISOString();
                console.log(url + '?datetime=' + currentDate);
                const response = await fetch(url + '?datetime=' + currentDate);

                if (response.ok) {
                    const messages = await response.json();
                    setMessages(messages);
                }
            };
            fetchData().catch(e => console.error(e));
        }, 2000);
        return () => clearInterval(interval);
    }, []);

    const sendRequestAddMessage = () => {
        const fetchData = async () => {
            const data = new URLSearchParams();
            data.set('message', message);
            data.set('author', 'Aijan Nogoeva');
            const response = await fetch(url, {
                method: 'post',
                body: data,
            });

            if (response.ok) {
                const messages = await response.json();
                setMessages(messages);
            }
        };
        fetchData().catch(e => console.error(e));
    }
    return (
        <div className="App">
            <div className="container">
                <div className="add_message">
                    <Form message={message} setMessage={setMessage} sendRequestAddMessage={sendRequestAddMessage}/>
                </div>
                <br/>
                <div className="all_messages">
                    {messages.map(message => (
                        <Post key={message._id} message={message.message} author={message.author}
                              datetime={message.datetime}/>
                    ))}
                </div>
            </div>
        </div>
    )
};
export default App;
